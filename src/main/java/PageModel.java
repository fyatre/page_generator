import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

public class PageModel {
//don't worry about platform
    @JsonProperty
    private String element;

    @JsonProperty
    private Map<String, String> types;

    @JsonProperty
    private List<String> attributes;

    public PageModel() {}


    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }



    public Map<String, String> getTypes() {
        return types;
    }

    public void setTypes(Map<String, String> types) {
        this.types = types;
    }



    public List<String> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }


    @Override
    public String toString() {
        return "Page Model Config: [element=" + element + ", types=" + types + ", attributes=" + attributes +"]";
    }



}
