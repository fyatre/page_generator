import com.beust.jcommander.JCommander;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;


public class Main {

    private static Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        logger.log(Level.INFO, "Initializing");

        Args options = new Args();
        options.exportDefaults();
        JCommander.newBuilder()
                .addObject(options)
                .build()
                .parse(args);


        PageBuilder pageBuilder = new PageBuilder(options.getName(), options.getPageModel());
        pageBuilder.loadXml(options.getPageSourceXML());
        pageBuilder.collectElementInfo();
        pageBuilder.GenerateExamplesHTML();

    }


}




