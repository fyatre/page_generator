import org.apache.commons.text.CaseUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PageBuilder {

    private static final Logger logger = Logger.getLogger(PageBuilder.class.getName());

    private Document document;
    private final String pageName;
    private final PageModel pageModel;

    private List<ElementInfo> infoList;

    private class ElementInfo {
        String locator = "";
        String getter = "";
        String xpath = "";
        String type = "";

        @Override
        public String toString(){
            return String.format("locator: %s%n%n" +
                    "getter: %s%n%n" +
                    "path: %s%n%n" +
                    "type: %s", locator, getter, xpath, type);
        }
    }


    protected Document getDocument(){
        return document;
    }



    public PageBuilder(String _name, PageModel _pageModel){
        this.pageName = _name;
        this.pageModel = _pageModel;


    }


    /**
     * Parse the given page source xml in to a list of interesting elements
     * @param sourceFile
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     * @throws XPathExpressionException
     */
    public void loadXml(File sourceFile) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;

        dBuilder = dbFactory.newDocumentBuilder();

        Document doc = dBuilder.parse(sourceFile);
        doc.getDocumentElement().normalize();

        this.document = doc;
    }


    /**
     * Populate the list of elements from the page
     * @throws XPathExpressionException
     */
    public void collectElementInfo() throws XPathExpressionException {

        infoList = new ArrayList<>();

        for (Map.Entry<String, String> type : pageModel.getTypes().entrySet()){
            NodeList nodes = getNodes(type.getValue());
            for (int i = 0; i < nodes.getLength(); i++){
                Element element = (Element) nodes.item(i);
                ElementInfo eInfo = new ElementInfo();

                eInfo.type = type.getKey();
                eInfo.getter = generateGetter(getLocatorKey(element), type.getKey());
                eInfo.xpath = getShortPath(element);
                eInfo.locator = String.format("%s = {\\\"locator\\\":\\\"xpath=%s\\\",\\\"desc\\\":\\\"%s\\\"}",
                        getLocatorKey(element),getShortPath(element),"");

                infoList.add(eInfo);
                logger.log(Level.INFO, eInfo + "\n------------------------");


            }

        }

    }


    public NodeList getNodes(String expression) throws XPathExpressionException {
        XPath xPath =  XPathFactory.newInstance().newXPath();
        return (NodeList) xPath.compile(expression).evaluate(
                getDocument(), XPathConstants.NODESET);
    }



    public String dotifyString(String original){
        String newString = original.split(",")[0]; //TODO: Does this belong here?

        String[] swap = {"-", "_", ":", " "};
        for(String s : swap ){
            newString = newString.replace(s, ".");
        }

        return newString;

    }


    public String getShortPath(Element element) throws XPathExpressionException {

        Map<String, String> attributes = getAttributeValues(element, pageModel.getAttributes());


        String baseString = "//" + element.getNodeName();
        boolean first = true;
        StringBuilder insert = new StringBuilder();


        if(getNodes(baseString).getLength() == 1){
            return baseString;
        }


        //Try adding each attribute one by one until we get a unique element
        for (Map.Entry<String, String> entry : attributes.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();

            if(!first){
                insert.append(" and ");
            }
            if(first){
                insert.append("[");
                first = false;
            }

            insert.append(String.format("@%s='%s'", key, value));

            String expression = baseString + insert + "]";

//            System.out.println("expression: " + expression);


            if(getNodes(expression).getLength() == 1){
                return expression;
            }


            // ...
        }

        return getXPath(element);




    }




    public static Map<String, String> getAttributeValues(Element element, List<String> attributes){
        Map<String, String> valueMap = new HashMap<>();

        for(String a : attributes){
            if(element.hasAttribute(a)){
                String attribute = element.getAttribute(a);
                if(!attribute.isEmpty()){
                    valueMap.put(a, element.getAttribute(a));
                }
            }
        }


        return valueMap;
    }


    public List<String> getBestAttributes(Element element){
        return pageModel.getAttributes();
    }

//    public abstract String extractLocator(Node node);

    public static String getXPath(Node node) {
        Node parent = node.getParentNode();
        if (parent == null || parent.getNodeName().equalsIgnoreCase("AppiumAUT" )) {
            return node.getNodeName();
        }
        return getXPath(parent) + "/" + node.getNodeName();
    }

    public void logAttributes(Element element){
        for(String a : pageModel.getAttributes()){
            if(element.hasAttribute(a)){
                if(!element.getAttribute(a).isEmpty()){
                    System.out.println(a + ":" + element.getAttribute(a));
                }

            }
        }

    }

    private String getLocatorKey(Element element){
        List<String> attributes = getBestAttributes(element);
        for (String a : attributes){
            if(element.hasAttribute(a)){
                return dotifyString(pageName + "." + element.getAttribute(a));
            }
        }
        return "unknown.element";
    }
//
//    private void logExamples(Element element) throws XPathExpressionException {
//        String elementName = getLocatorKey(element);
//        String path = getShortPath(element);
//        String locator = String.format("%s = {\\\"locator\\\":\\\"xpath=%s\\\",\\\"desc\\\":\\\"%s\\\"}",elementName,path,"");
//        System.out.println(locator);
//        System.out.println("");
//        System.out.println(generateGetter(elementName));
//    }


    private String getAllGetters(){

        StringBuilder result = new StringBuilder();

        for(ElementInfo e : infoList){
            result.append(e.getter).append("\n\n");
        }

        return result.toString();
    }

    private String getAllLocators() {
        StringBuilder result = new StringBuilder();
        for(ElementInfo e : infoList){
            result.append(e.locator).append("\n");

        }
        return result.toString();
    }

    private String generateGetter(String elementName, String type){
        String newString = elementName;
        if(!newString.endsWith(type)){
            newString += "." + type;
        }
        String[] swap = {"-", "_", ":", "."};
        for(String s : swap ){
            newString = newString.replace(s, " ");
        }

        String varName = CaseUtils.toCamelCase(newString, false, ' ');
        String methodName = CaseUtils.toCamelCase(newString, true, ' ');


        return String.format("private QAFWebElement %s;%n", varName ) +
                String.format("public QAFWebElement get%s() {%n", methodName) +
                String.format("    return %s;%n}", varName);

    }



    public void logXpath(Element element) throws XPathExpressionException {
        System.out.println(getShortPath(element));
    }

//    public void logNode(Node node) throws XPathExpressionException {
//        if (node.getNodeType() == Node.ELEMENT_NODE) {
//            Element eElement = (Element) node;
////            logAttributes(eElement);
//            logXpath(eElement);
//            System.out.println("");
//            logExamples(eElement);
//
//            System.out.println("----");
//
//        }
//    }


//    public void logNodeList(NodeList nodes) throws XPathExpressionException {
//        for (int i = 0; i < nodes.getLength(); i++) {
//            Node nNode = nodes.item(i);
//            logNode(nNode);
//        }
//    }

    /**
     * TODO: This is probably not the optimal way to do this
     */
    public void GenerateExamplesHTML() throws XPathExpressionException {

        String templateFileName = "templates/examples.html";
        String outFileName = "default.html";




//        InputStream inputStream = getClass().getResourceAsStream(templateFileName);

        try {
            String text = new Scanner(Objects.requireNonNull(PageBuilder.class.getResourceAsStream(templateFileName)), "UTF-8").useDelimiter("\\A").next();
            text = text.replace("CLASS_EXAMPLE_HERE", getAllGetters());
            text = text.replace("LOCATOR_EXAMPLE_HERE", getAllLocators());
            logger.log(Level.INFO, text);

            BufferedWriter writer = new BufferedWriter(new FileWriter(outFileName));
            writer.write(text);

            writer.close();


        }catch (Exception e){
            logger.log(Level.SEVERE, e.getMessage());
        }

    }




}
