import com.beust.jcommander.Parameter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Args {
    private static Logger logger = Logger.getLogger(Args.class.getName());

    @Parameter
    private List<String> parameters = new ArrayList<>();

    @Parameter(names = { "-s", "--page-source" }, description = "Page Source xml", required = true)
    private String source = null;

    @Parameter(names = { "-n", "--page-name" }, description = "")
    private String name = "default.page";

    @Parameter(names = { "-m", "--page-model" }, description = "Config for how to parse the page")
    private String pageModel = "default.yml";

    public List<String> getParameters() {
        return parameters;
    }

    private String defaultPageModel = "default.yml";


    public File getPageSourceXML() throws FileNotFoundException {
        return new File(source);
    }

    private InputStream loadResource(String file){
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        return classloader.getResourceAsStream(file);
    }

    public String getName() {
        return name;
    }

    public PageModel getPageModel() throws IOException {
        try {
            return readYaml(new File(pageModel));
        }catch(Exception e){
            logger.log(Level.SEVERE,e.getMessage());
            System.exit(1);
        }
        return null;
    }

    public static PageModel readYaml(final File file) throws IOException {
        final ObjectMapper mapper = new ObjectMapper(new YAMLFactory()); // jackson databind
        return mapper.readValue(file, PageModel.class);
    }


    public void exportDefaults(){
        InputStream defaultModel = getClass().getResourceAsStream("models/default.yml");
        copy(defaultModel, "default.yml");
    }

    public static boolean copy(InputStream source , String destination) {
        boolean succeess = true;

        System.out.println("Copying ->" + source + "\n\tto ->" + destination);

        if(new File("destination").exists()){
           return false;
        }

        try {
            Files.copy(source, Paths.get(destination));
        } catch (IOException ex) {
//            logger.log(Level.WARNING, "", ex);
            succeess = false;
        }

        return succeess;

    }
}
