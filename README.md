# Page Generator

This tool takes a page source xml file, as you might obtain from Perfecto/Appium, and pulls out the interesting elements.
With these the tool will create snippets for adding to a Page class, and locator file.

## Example:

```
java -jar page_generator.jar -p loginPage.xml -n login.page 
```

```
##################### input #####################
//android.widget.EditText[@content-desc='sign_in:input:email:textInput']

default.page.sign.in.input.email.textInput = {\"locator\":\"xpath=//android.widget.EditText[@content-desc='sign_in:input:email:textInput']\",\"desc\":\"\"}

private QAFWebElement defaultPageSignInInputEmailTextinput;
public QAFWebElement getDefaultPageSignInInputEmailTextinput() {
    return defaultPageSignInInputEmailTextinput;
}
----
//android.widget.EditText[@content-desc='sign_in:input:password:textInput']

default.page.sign.in.input.password.textInput = {\"locator\":\"xpath=//android.widget.EditText[@content-desc='sign_in:input:password:textInput']\",\"desc\":\"\"}

private QAFWebElement defaultPageSignInInputPasswordTextinput;
public QAFWebElement getDefaultPageSignInInputPasswordTextinput() {
    return defaultPageSignInInputPasswordTextinput;
}
----

##################### text #####################
//android.widget.TextView[@content-desc='sign_in:input:password:maskText']

login.page.sign.in.input.password.maskText = {\"locator\":\"xpath=//android.widget.TextView[@content-desc='sign_in:input:password:maskText']\",\"desc\":\"\"}

private QAFWebElement loginPageSignInInputPasswordMasktext;
public QAFWebElement getLoginPageSignInInputPasswordMasktext() {
    return loginPageSignInInputPasswordMasktext;
}
----
//android.widget.TextView[@content-desc='sign_in:button:sign_in:button:title']

login.page.sign.in.button.sign.in.button.title = {\"locator\":\"xpath=//android.widget.TextView[@content-desc='sign_in:button:sign_in:button:title']\",\"desc\":\"\"}

private QAFWebElement loginPageSignInButtonSignInButtonTitle;
public QAFWebElement getLoginPageSignInButtonSignInButtonTitle() {
    return loginPageSignInButtonSignInButtonTitle;
}

```



## Page Model

In order to configure how elements are found, a page model yaml file can be provided

```
java -jar page_generator.jar -p loginPage.xml -n login.page -m default.yml
```

A default will be generated if it hasn't yet.

```yaml
element: "QAFWebElement"
types:
  buttons: "//android.widget.Button|//*[contains(@name,'button')]"
  text: "//android.widget.TextView|//XCUIElementTypeStaticText"
  switch: "//android.widget.Switch|//XCUIElementTypeSwitch|//*[contains(name, 'switch')]"
  input: "//android.widget.EditText|//XCUIElementTypeTextField|//XCUIElementTypeSecureTextField"
  password: "//android.widget.EditText[@password='true']|//XCUIElementTypeSecureTextField"
  images: "//android.widget.ImageView|//XCUIElementTypeImage"
attributes:
  - "id"
  - "identifier"
  - "content-desc"
  - "name"
  - "label"
  - "text"
```

`element` is the object type that will be used when creating the getter template

```java
private QAFWebElement loginPageSignInInputPasswordTextinput;
public QAFWebElement getLoginPageSignInInputPasswordTextinput() {
    return loginPageSignInInputPasswordTextinput;
}
```

`types` are a list of xpaths that are grouped by their basic object type.
This items in this list can be modified, removed, or added as desired. 
The key will be used to group the elements.

Multiple potential paths can be grouped together with `|`

```
  password: "//android.widget.EditText[@password='true']|//XCUIElementTypeSecureTextField"
```


`attributes` is a priority list of attribute tags that will be used to uniquely identify an element in order to create the short xpath.
If the element is found to not be unique, the next attribute in the list will be added and tested until it is.
If an element somehow still has the same attributes as another, the raw xpath will be returned.

```
//android.widget.EditText[@content-desc='sign_in:input:password:textInput']
```